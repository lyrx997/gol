package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class CellTest {

	@Test
	public void cellShouldBeAlive() throws CustomLifeException {
		
		Cell cell = new Cell(0, 0, true);
		
		assertTrue(cell.isAlive());
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseException() throws CustomLifeException {
		
		new Cell(-1, 0, true);
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellWithNegativeYPositionShouldRaiseException() throws CustomLifeException {
		
		new Cell(0, -1, true);
	}
	
	@Test
	public void cellShouldBeDead() throws CustomLifeException {
		
		Cell cell = new Cell(0, 0, true);
		
		cell.setAlive(false);
		
		assertFalse(cell.isAlive());
	}
	
	@Test
	public void cellShouldReturnX() throws CustomLifeException {
		
		Cell cell = new Cell(5, 0, true);
		
		assertEquals(5, cell.getX());
	}
	
	@Test
	public void cellShouldReturnY() throws CustomLifeException {
		
		Cell cell = new Cell(0, 5, true);
		
		assertEquals(5, cell.getY());
	}
	
	@Test
	public void cellShouldHaveAliveNeighbors() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, false);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, true);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, true);
		cellMatrix[2][1] = new Cell(2, 1, true);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertEquals(3, cellMatrix[1][1].getNumberOfAliveNeighbords() );
		
	}
	
	@Test
	public void aliveCellWithThreeNeighboarsShouldSurvive() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, false);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, true);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, true);
		cellMatrix[2][1] = new Cell(2, 1, true);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertTrue( cellMatrix[1][1].willSurvive() );
		
	}
	
	@Test
	public void aliveCellWithTwoNeighboarsShouldSurvive() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, false);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, true);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, true);
		cellMatrix[2][1] = new Cell(2, 1, false);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertTrue( cellMatrix[1][1].willSurvive() );
		
	}
	
	@Test
	public void aliveCellShouldNotSurvive() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, false);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, true);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, false);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertFalse( cellMatrix[1][1].willSurvive() );
		
	}
	
	@Test
	public void deadCellShouldNotSurvive() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, false);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, false);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, false);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertFalse( cellMatrix[1][1].willSurvive() );
		
	}
	
	@Test
	public void aliveCellWithMoreThanThreeNeighboarsShouldDie() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, true);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, true);
		cellMatrix[1][1] = new Cell(1, 1, true);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, true);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertTrue( cellMatrix[1][1].willDie() );
		
	}
	
	@Test
	public void aliveCellWithLessThanTwoNeighboarsShouldDie() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, false);
		cellMatrix[0][2] = new Cell(0, 2, false);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, true);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, true);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertTrue( cellMatrix[1][1].willDie() );
		
	}
	
	@Test
	public void aliveCellShouldNotDie() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, true);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, true);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, true);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertFalse( cellMatrix[1][1].willDie() );
		
	}
	
	@Test
	public void deadCellShouldNotDie() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, true);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, false);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, true);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertFalse( cellMatrix[1][1].willDie() );
		
	}
	
	@Test
	public void deadCellShouldRevive() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, true);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, false);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, true);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertTrue( cellMatrix[1][1].willRevive() );
		
	}

	@Test
	public void deadCellShouldNotRevive() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, true);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, false);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, false);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertFalse( cellMatrix[1][1].willRevive() );
		
	}
	
	@Test
	public void deadCellWithLessThanThreeNeighboarsAliveShouldNotRevive() throws CustomLifeException {
		
		Cell[][] cellMatrix = new Cell[3][3];
		
		cellMatrix[0][0] = new Cell(0, 0, false);
		cellMatrix[0][1] = new Cell(0, 1, true);
		cellMatrix[0][2] = new Cell(0, 2, true);
		
		cellMatrix[1][0] = new Cell(1, 0, false);
		cellMatrix[1][1] = new Cell(1, 1, false);
		cellMatrix[1][2] = new Cell(1, 2, false);
		
		cellMatrix[2][0] = new Cell(2, 0, false);
		cellMatrix[2][1] = new Cell(2, 1, false);
		cellMatrix[2][2] = new Cell(2, 2, false);
		
		cellMatrix[1][1].setNumberOfAliveNeighbors(cellMatrix);
		
		assertFalse( cellMatrix[1][1].willRevive() );
		
	}
	
	@Test
	public void cellsShouldNotBeNeighboar() throws CustomLifeException {
		
		Cell c1 = new Cell(0, 0, true);
		Cell c2 = new Cell(2, 2, true);
		
		assertFalse( c1.isNeighboard(c2) );
		
	}
	
	@Test
	public void cellsShouldNotBeNeighboarWithDifferentPositions() throws CustomLifeException {
		
		Cell c1 = new Cell(0, 0, true);
		Cell c2 = new Cell(0, 2, true);
		
		assertFalse( c1.isNeighboard(c2) );
		
	}

}
